libcf
=====

1. File I/O
1. TCP Socket
1. Logging (advisor: vfire)
1. Utility Code for Debugging
1. Thread & Mutex
1. Codec
  1. hex encoding/decoding
  1. base64 encoding/decoding
1. Data Structure
  1. Linked-List
  1. Queue
  1. Stack
1. Utilities
  1. Date&Time
  1. Getting System Error Code

..more needs ?
