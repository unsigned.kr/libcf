/**
 * \file cf_stack.c
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 스택 구현
 */
#include "cf_stack.h"
#include "cf_list.h"
#include "cf_local.h"
#include "cf_error.h"

#include <stdio.h>

static int
CF_Stack_Local_Get (cf_ctx	ctx,
					void	** element,
					CF_BOOL	removeFlag)
{
	int				result = 0;
	cf_ctx			list = (cf_ctx) ctx;
	cf_traverser	trav = NULL;

	result = CF_List_Front (list, &trav);
	if (result < 0)
		return result;

	result = CF_List_Get (trav, element);
	if (result < 0)
		return result;

	result = (removeFlag == CF_TRUE)
		   ? CF_List_Remove (ctx, &trav)
		   : CF_OK;

	return result;
}

/**
 * 스택 컨텍스트 생성
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx 스택 컨텍스트 포인터
 */
int
CF_Stack_Create (cf_ctx * ctx)
{
	return CF_List_Create ((cf_ctx *) ctx);
}

/**
 * 스택 컨텍스트 해제
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx 스택 컨텍스트
 */
int
CF_Stack_Destroy (cf_ctx ctx)
{
	return CF_List_Destroy (ctx);
}

/**
 * 스택에 삽입
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx		스택 컨텍스트
 * \param element	데이터 주소
 */
int
CF_Stack_Push (cf_ctx		ctx,
			   const void	* element)
{
	cf_ctx list = (cf_ctx) ctx;

	return CF_List_AddFront (list, element);
}

/**
 * 스택에서 꺼내기
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx		스택 컨텍스트
 * \param element	데이터 주소
 */
int
CF_Stack_Pop (cf_ctx	ctx,
			  void		** element)
{
	return CF_Stack_Local_Get (ctx, element, CF_TRUE);
}

/**
 * 스택 최상위의 데이터 조회
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx		스택 컨텍스트
 * \param element	데이터 주소
 */
int
CF_Stack_Top (cf_ctx 	ctx,
			  void		** element)
{
	return CF_Stack_Local_Get (ctx, element, CF_FALSE);
}

/**
 * 스택에 등록된 항목의 수를 가져옴
 *
 * \return 성공 시, 항목 수; 실패 시, 오류 코드
 *
 * \param ctx 스택 컨텍스트
 */
int
CF_Stack_GetSize (cf_ctx ctx)
{
	return CF_List_GetSize ((cf_ctx) ctx);
}
