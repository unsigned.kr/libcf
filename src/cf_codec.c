/**
 * \file cf_codec.c
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 데이터 변환 구현
 */
#include "cf_codec.h"
#include "cf_type.h"
#include "cf_error.h"

#include <string.h>
#include <stdio.h>

#define BASE64_PADDING_CHAR_INDEX	64

#define ASSERT_ARGS(x)	\
	if ((x))			\
		return CF_ERROR_CODEC_INVALID_ARGS

/**
 * hex-encode
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param bin	바이너리 데이터
 * \param len	바이너리 데이터의 길이
 * \param hex	16진수 문자열을 저장할 주소
 *
 * \remarks
 * hex는 할당된 메모리이며, 크기는 '\0'를 제외하고 len * 2
 */
int
CF_Codec_Hex_Encode (const cf_byte	* bin,
					 const size_t	len,
					 char			* hex)
{
	const cf_byte	* src = bin;
	char			* dst = hex;

	const static char	hexenc[] = {
		'0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
	};

	ASSERT_ARGS (bin == NULL);
	ASSERT_ARGS (hex == NULL);

	for ( ; (size_t)(src - bin) < len ; src++)
	{
		*dst++ = hexenc[((*src) >> 4) & 0x0f];
		*dst++ = hexenc[((*src)     ) & 0x0f];
	}
	*dst = '\0';

	return CF_OK;
}

/**
 * hex-decode
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param hex	16진수 문자열
 * \param bin	바이너리 데이터를 저장할 주소
 * \param len	바이너리 데이터의 길이를 저장할 주소
 *
 * \remarks
 * bin는 할당된 메모리이며, 크기는 strlen (hex) / 2
 */
int
CF_Codec_Hex_Decode (const char	* hex,
					 cf_byte	* bin,
					 size_t		* len)
{
	size_t		length = 0;	/* absolutely even-number */

	const char	* src = hex;
	cf_byte		* dst = bin;
	char		buf = 0;
	cf_byte		val = 0;
	cf_byte		asciiHex = 0;

	const static cf_byte hexdec[] = {
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  00 -  07 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  08 -  15 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  16 -  23 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  24 -  31 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  32 -  39 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  40 -  47 */
		 '0', '0', '0', '0', '0', '0', '0', '0',	/*  48 -  55 */
		 '0', '0',0x00,0x00,0x00,0x00,0x00,0x00,	/*  56 -  63 */
		0x00, 'A', 'A', 'A', 'A', 'A', 'A',0x00,	/*  64 -  71 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  71 -  79 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  80 -  87 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/*  88 -  95 */
		0x00, 'a', 'a', 'a', 'a', 'a', 'a',0x00,	/*  96 - 103 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/* 104 - 111 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,	/* 112 - 119 */
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00		/* 120 - 127 */
	};

	ASSERT_ARGS (hex == NULL);
	ASSERT_ARGS (bin == NULL);
	ASSERT_ARGS (len == NULL);

	for ( ; hexdec[(int)*src] && *src ; src++, length++);

	if (*src)
		return CF_ERROR_CODEC_NOT_HEXSTRING;

	for (src = hex ; *src ; )
	{
		val = 0;	/* init/re-init docoding-buffer */

		/* decode one character */
#define DECODE_HEX(x)					\
		do {							\
		buf = *(x);						\
		val = (cf_byte)(val << 4);		\
		asciiHex = hexdec[(int)buf];	\
		\
		val |=	(cf_byte)				\
				(buf - asciiHex + (asciiHex == '0' ? 0 : 10));	\
		} while (0)

		/* decode one byte by decode two character */
		DECODE_HEX (src++);
		DECODE_HEX (src++);

		*dst++ = val;
	}
	*len = length / 2;

	return CF_OK;
}

/**
 * Base64-encode
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param bin		바이너리 데이터
 * \param len		바이너리 데이터 길이
 * \param base64	base64 문자열을 저장할 주소
 *
 * \remarks
 * base64는 할당된 메모리이며, 크기는 '\0'를 제외하고 ((len + 2) / 3) * 4
 */
int
CF_Codec_Base64_Encode (const cf_byte	* bin,
						const size_t	len,
						char			* base64)
{
	const cf_byte	* src = bin;
	char			* dst = base64;

	const static char base64enc[] = {
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',	/* 00 - 07 */
		'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',	/* 08 - 15 */
		'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',	/* 16 - 23 */
		'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',	/* 24 - 31 */
		'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',	/* 32 - 39 */
		'o', 'p', 'q', 'r', 's', 't', 'u', 'v',	/* 40 - 47 */
		'w', 'x', 'y', 'z', '0', '1', '2', '3',	/* 48 - 55 */
		'4', '5', '6', '7', '8', '9', '+', '/',	/* 56 - 63 */
		'='										/* padding */
	};

	ASSERT_ARGS (src == NULL);
	ASSERT_ARGS (dst == NULL);

#define SEXTUPLE_E_1(__x)	(((__x[0]) >> 2) & 0x3f)
#define SEXTUPLE_E_2(__x)	(((__x[0]) << 4) & 0x30)
#define SEXTUPLE_E_3(__x)	(((__x[1]) >> 4) & 0x0f)
#define SEXTUPLE_E_4(__x)	(((__x[1]) << 2) & 0x3c)
#define SEXTUPLE_E_5(__x)	(((__x[2]) >> 6) & 0x03)
#define SEXTUPLE_E_6(__x)	(((__x[2])     ) & 0x3f)

	for ( ; (size_t)(src - bin) < len - 2 ; src += 3)
	{
		*dst++ = base64enc[SEXTUPLE_E_1 (src)];
		*dst++ = base64enc[SEXTUPLE_E_2 (src) | SEXTUPLE_E_3 (src)];
		*dst++ = base64enc[SEXTUPLE_E_4 (src) | SEXTUPLE_E_5 (src)];
		*dst++ = base64enc[SEXTUPLE_E_6 (src)];
	}

	if ((size_t)(src - bin) < len)
	{
		*dst++ = base64enc[SEXTUPLE_E_1 (src)];

		if ((size_t) (src - bin) == len - 1)
		{
			*dst++ = base64enc[SEXTUPLE_E_2 (src)];
			*dst++ = base64enc[BASE64_PADDING_CHAR_INDEX];
		}
		else
		{
			*dst++ = base64enc[SEXTUPLE_E_2 (src) | SEXTUPLE_E_3 (src)];
			*dst++ = base64enc[SEXTUPLE_E_4 (src)];
		}

		*dst++ = base64enc[BASE64_PADDING_CHAR_INDEX];
	}
	*dst = '\0';

	return CF_OK;
}

/**
 * Base64-decode
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param base64	base64 문자열
 * \param bin		바이너리 데이터를 저장할 주소
 * \param len		바이너리 데이터의 길이를 저장할 주소
 *
 * \remarks
 * base64는 할당된 메모리이며, 크기는 (strlen (base64)) / 4 * 3
 */
int
CF_Codec_Base64_Decode (const char	* base64,
						cf_byte		* bin,
						size_t		* len)
{
	const char	* src = base64;
	cf_byte		* dst = bin;
	size_t		remain = 0;
	size_t		binlen = 0;

	const static cf_byte base64dec[] = {
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,	/*  00 -  07 */
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,	/*  08 -  15 */
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,	/*  16 -  23 */
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,	/*  24 -  31 */
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,	/*  32 -  39 */
		0xff,0xff,0xff,  62,0xff,0xff,0xff,  63,	/*  40 -  47 */
		  52,  53,  54,  55,  56,  57,  58,  59,	/*  48 -  55 */
		  60,  61,0xff,0xff,0xff,  64,0xff,0xff,	/*  56 -  63 */
		0xff,   0,   1,   2,   3,   4,   5,   6,	/*  64 -  71 */
	 	   7,   8,   9,  10,  11,  12,  13,  14,	/*  71 -  79 */
		  15,  16,  17,  18,  19,  20,  21,  22,	/*  80 -  87 */
		  23,  24,  25,0xff,0xff,0xff,0xff,0xff,	/*  88 -  95 */
		0xff,  26,  27,  28,  29,  30,  31,  32,	/*  96 - 103 */
	  	  33,  34,  35,  36,  37,  38,  39,  40,	/* 104 - 111 */
		  41,  42,  43,  44,  45,  46,  47,  48,	/* 112 - 119 */
		  49,  50,  51,0xff,0xff,0xff,0xff,0xff		/* 120 - 127 */
	};

	ASSERT_ARGS (src == NULL);
	ASSERT_ARGS (dst == NULL);
	ASSERT_ARGS (len == NULL);

	while (base64dec[(int)*src] < BASE64_PADDING_CHAR_INDEX) src++;

	if (*src == 0xff)
		return CF_ERROR_CODEC_NOT_BASE64;

	remain = (size_t)(src - base64);
	binlen = ((remain + 2/* max padding length */) / 4) * 3;

#define SEXTUPLE_D_1(src)	(base64dec[(int)src[0]] << 2)
#define SEXTUPLE_D_2(src)	(base64dec[(int)src[1]] >> 4)
#define SEXTUPLE_D_3(src)	(base64dec[(int)src[1]] << 4)
#define SEXTUPLE_D_4(src)	(base64dec[(int)src[2]] >> 2)
#define SEXTUPLE_D_5(src)	(base64dec[(int)src[2]] << 6)
#define SEXTUPLE_D_6(src)	(base64dec[(int)src[3]]     )

	for (src = base64 ; remain > 4 ; remain -= 4, src += 4)
	{
		*dst++ = (cf_byte)(SEXTUPLE_D_1 (src) | SEXTUPLE_D_2 (src));
		*dst++ = (cf_byte)(SEXTUPLE_D_3 (src) | SEXTUPLE_D_4 (src));
		*dst++ = (cf_byte)(SEXTUPLE_D_5 (src) | SEXTUPLE_D_6 (src));
	}

	if (remain > 1)
		*dst++ = (cf_byte)(SEXTUPLE_D_1 (src) | SEXTUPLE_D_2 (src));
	if (remain > 2)
		*dst++ = (cf_byte)(SEXTUPLE_D_3 (src) | SEXTUPLE_D_4 (src));
	if (remain > 3)
		*dst++ = (cf_byte)(SEXTUPLE_D_5 (src) | SEXTUPLE_D_6 (src));

	*len = binlen - (4 - remain);

	return CF_OK;
}
