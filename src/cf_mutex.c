/**
 * \file cf_mutex.c
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 뮤텍스 구현
 */
#include "cf_mutex.h"
#include "cf_local.h"
#include "cf_error.h"

#include <stdio.h>
#include <stdlib.h>

#if defined(_WIN32) || defined(_WIN64)
# include <windows.h>
# include <process.h>
# define MUTEX_TYPE		CRITICAL_SECTION
#else // #if defined(_WIN32) || defined(_WIN64)
# include <pthread.h>
# define MUTEX_TYPE		pthread_mutex_t
#endif // #if defined(_WIN32) || defined(_WIN64)

#define ASSERT_CTX(__ctx)	\
	if (__ctx == NULL)		\
		return CF_ERROR_MUTEX_INVALID_CTX

typedef struct __cf_ctx_ctx__
{
	MUTEX_TYPE	mid;
} CF_MUTEX_CONTEXT;

/**
 * 뮤텍스 컨텍스트를 생성
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx 뮤텍스 컨텍스트 주소
 */
int
CF_Mutex_Create (cf_ctx * ctx)
{
	int result = 0;

	CF_MUTEX_CONTEXT * context = NULL;

	ASSERT_CTX (ctx);

	context = NEWCTX (CF_MUTEX_CONTEXT);
	if (context == NULL)
		return CF_ERROR_MUTEX_CREATE_CTX;

	TRY
	{
#if defined(_WIN32) || defined(_WIN64)
		InitializeCriticalSection (&context->mid);
#else
		result = pthread_mutex_init (&context->mid, NULL);
		if (result)
		{
			result = CF_ERROR_MUTEX_CREATE;
			TRY_BREAK;
		}
#endif

		*ctx = context;
	}
	CATCH_IF (result < 0)
	{
		CF_Mutex_Destory ((cf_ctx) context);
	}

	return result;
}

/**
 * 뮤텍스 컨텍스트 해제
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx 뮤텍스 컨텍스트
 */
int
CF_Mutex_Destory (cf_ctx ctx)
{
	CF_MUTEX_CONTEXT * context = (CF_MUTEX_CONTEXT *) ctx;

	ASSERT_CTX (ctx);

#if defined(_WIN32) || defined(_WIN64)
	DeleteCriticalSection (&context->mid);
#else
	pthread_mutex_destroy (&context->mid);
#endif

	free (context);

	return CF_OK;
}

/**
 * 뮤텍스 잠금
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx 뮤텍스 컨텍스트
 */
int
CF_Mutex_Lock (cf_ctx ctx)
{
	CF_MUTEX_CONTEXT * context = (CF_MUTEX_CONTEXT *) ctx;

	ASSERT_CTX (ctx);

#if defined(_WIN32) || defined(_WIN64)
	BOOL st;
	st = EnterCriticalSection (&context->mid);
	fprintf (stderr, "%d ", st);
#else
	pthread_mutex_lock (&context->mid);
#endif

	return CF_OK;
}

/**
 * 뮤텍스 잠금 해제
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param ctx 뮤텍스 컨텍스트
 */
int
CF_Mutex_Unlock (cf_ctx ctx)
{
	CF_MUTEX_CONTEXT * context = (CF_MUTEX_CONTEXT *) ctx;

	ASSERT_CTX (ctx);

#if defined(_WIN32) || defined(_WIN64)
	LeaveCriticalSection (&context->mid);
#else
	pthread_mutex_unlock (&context->mid);
#endif

	return CF_OK;
}
