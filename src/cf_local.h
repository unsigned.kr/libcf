/**
 * \file cf_local.h
 *
 * \author myusgun <myusgun@gmail.com>
 */
#ifndef __CF_LOCAL_H__
#define __CF_LOCAL_H__

#include <stdlib.h>

/** 파일 이름 및 경로 길이 */
#define NAME_LENGTH						128

/** 예외 처리 */
#define TRY						do
#define TRY_BREAK				break
#define NO_CATCH				while (0)
#define CATCH_IF(__expr)		NO_CATCH; if (__expr)
#define CATCH_ELSE_IF(__expr)	else if (__expr)
#define CATCH_ELSE				else

/**
 * 유틸 매크로
 *
 * \remarks
 * header는 별도로 include 해야 함
 */
#define NEWCTX(__ctx)			((__ctx *) calloc (sizeof (__ctx), 1))

/** non-compatible function */
#if defined(_WIN32) || defined(_WIN64)
# define snprintf		_snprintf
#else
#endif

#endif // #ifndef __CF_LOCAL_H__
