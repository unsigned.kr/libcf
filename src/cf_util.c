/**
 * \file cf_util.c
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 유틸 구현
 */
#include "cf_util.h"
#include "cf_error.h"
#include "cf_local.h"

#include <stdio.h>
#include <time.h>

#if defined(_WIN32) || defined(_WIN64)
# include <windows.h>
#else
# include <sys/time.h>
# include <errno.h>
#endif

#define ASSERT_ARGS(x)	\
	if ((x))			\
		return CF_ERROR_UTIL_INVALID_ARGS

#if defined(_WIN32) || defined(_WIN64)
/* {{{ */
struct timezone
{
	int tz_minuteswest;	/* minutes W of Greenwich */
	int tz_dsttime;		/* type of dst correction */
};

static int
gettimeofday (struct timeval *tv, struct timezone *tz)
{
	FILETIME			ft;
	unsigned __int64	buf =0;
	//static int tzflag = 0;

	if (NULL != tv)
	{
		GetSystemTimeAsFileTime (&ft);

		buf |=	ft.dwHighDateTime;
		buf <<=	32;
		buf |=	ft.dwLowDateTime;

		if (buf)
		{
			buf /= 10;
			buf -= ((369 * 365 + 89) * (unsigned __int64) 86400) * 1000000;
		}

		tv->tv_sec = (long)(buf / 1000000UL);
		tv->tv_usec = (long)(buf % 1000000UL);
	}

	/*
	if (NULL != tz)
	{
		if (!tzflag)
		{
			_tzset();
			tzflag++;
		}

		// Adjust for the timezone west of Greenwich
		tz->tz_minuteswest = _timezone / 60;
		tz->tz_dsttime = _daylight;
	}
	*/

	return 0;
}
/* }}} */
#endif

/**
 * 시스템 오류코드를 반환
 *
 * \return 시스템 오류코드
 *
 * \remarks
 * windows    : GetLastError () <br />
 * linux/unix : errno
 */
int
CF_Util_GetSystemError (void)
{
#if defined(_WIN32) || defined(_WIN64)
	return GetLastError ();
#else
	return errno;
#endif
}

/**
 * 현재 시간을 가져옴
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param dt	CF_UTIL_DATETIME 구조체 주소
 */
int
CF_Util_GetCurrentTime (CF_UTIL_DATETIME * dt)
{
	struct timeval	timeVal;
	struct tm		* timebuf;

	ASSERT_ARGS (dt == NULL);

	gettimeofday (&timeVal, NULL);
	timebuf = localtime ((const time_t *)&timeVal.tv_sec);
	if (timebuf == NULL)
		return CF_ERROR_UTIL_GET_LOCALTIME;

	timeVal.tv_usec /= 1000;

	dt->year	= timebuf->tm_year + 1900;
	dt->month	= timebuf->tm_mon + 1;
	dt->day		= timebuf->tm_mday;
	dt->week	= timebuf->tm_wday;

	dt->hour	= timebuf->tm_hour;
	dt->min		= timebuf->tm_min;
	dt->sec		= timebuf->tm_sec;

	dt->usec	= (int) timeVal.tv_usec;

	return CF_OK;
}

/**
 * 시간정보를 문자열로 변환
 *
 * \return 성공 시, CF_OK; 실패 시, 오류 코드
 *
 * \param dt	CF_UTIL_DATETIME 구조체 주소
 * \param str	변환한 문자열을 저장할 충분한 공간의 메모리
 *
 * \remarks
 * 날짜/시간 문자열 형식 : yyyy-MM-dd HH:mm:ss.SSS
 */
int
CF_Util_GetTimeString (const CF_UTIL_DATETIME	* dt,
					   char						* str)
{
	ASSERT_ARGS (dt  == NULL);
	ASSERT_ARGS (str == NULL);

	snprintf (str, CF_UTIL_DATETIME_LENGTH,
			  "%04d-%02d-%02d %02d:%02d:%02d.%03d",
			  dt->year, dt->month, dt->day,
			  dt->hour, dt->min, dt->sec, dt->usec);

	return CF_OK;
}
