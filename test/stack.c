/**
 * @file	stack.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_stack.h"
#include "cf_debug.h"

#define	COUNT	5

int main (void)
{
	long long int	iter = 0;
	int				result = 0;
	long long int	element = 0;
	cf_ctx			stack = NULL;

	// create
	result = CF_Stack_Create (&stack);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	// insert
	for (iter = 0 ; iter < COUNT ; iter++)
	{
		result = CF_Stack_Push (stack, (void *)iter);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);
	}

	// move next
	for (iter = 0 ; iter < COUNT ; iter++)
	{
		result = CF_Stack_Top (stack, (void **)&element);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);
		else
			CF_DEBUG_PRINT (stderr, "top   : %4d\n", element);

		result = CF_Stack_Pop (stack, (void **)&element);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);
		else
			CF_DEBUG_PRINT (stderr, "poped : %4d\n", element);

		CF_DEBUG_PRINT (stderr, "\n");
	}

	// destroy
	result = CF_Stack_Destroy (stack);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	return 0;
}
