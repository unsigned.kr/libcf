/**
 * @file	debug.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_file.h"
#include "cf_debug.h"

#include <string.h>

void callee3 (void)
{
	CF_DEBUG_BEGIN_FUNCTION;

	// ...
	CF_DEBUG_PRINT_CALLSTACK (stdout);
	// ...

	CF_DEBUG_END_FUNCTION;
}

void callee2 (void)
{
	CF_DEBUG_BEGIN_FUNCTION;

	// ...
	callee3 ();
	// ...

	CF_DEBUG_END_FUNCTION;
}

void callee1 (void)
{
	CF_DEBUG_BEGIN_FUNCTION;

	// ...
	callee2 ();
	// ...

	CF_DEBUG_END_FUNCTION;
}

int main (void)
{
	char data[] =
		"【 曲名 : 사랑하고 있는데 】\n"
		"《 歌 : Kylee 》\n"
		"『 절원의 템페스트 OP2 Theme 』\n";

	/* init. once */
	CF_DEBUG_INITIALIZE;

	/* at begin function */
	CF_DEBUG_BEGIN_FUNCTION;

	/* print */
	CF_DEBUG_PRINT (stderr, "print message with context to stderr\n");

	CF_DEBUG_PRINT_BIN (stdout, (unsigned char *) data, (int) sizeof (data), "data : \n%s", data);

	/* step into other function with cf debugging util */
	callee1 ();

	/* at leave function */
	CF_DEBUG_END_FUNCTION;

	CF_DEBUG_FINALIZE;

	return 0;
}
