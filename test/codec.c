/**
 * @file	codec.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_codec.h"
#include "cf_debug.h"
#include <stdio.h>
#include <string.h>

int main (void)
{
	char data[] = "ONE OK ROCK - Nothing Helps";

	char encode[512] = {0x00,};
	unsigned char bin[512] = {0x00,};
	size_t length = 0;

	/* hex */
	CF_DEBUG_PRINT (stderr, "------------------- codec/hex ----------------\n");
	CF_DEBUG_PRINT (stderr, "data   : %s\n", data);
	CF_DEBUG_PRINT (stderr, "= Convert binary to hex =\n");
	CF_Codec_Hex_Encode ((unsigned char *)data, strlen (data), encode);
	CF_DEBUG_PRINT (stderr, "hex    : %s\n", encode);
	CF_DEBUG_PRINT_BIN (stderr, (unsigned char *) data, strlen (data), "data   : %s\n", data);

	CF_DEBUG_PRINT (stderr, "= Convert hex to binary =\n");
	if (CF_Codec_Hex_Decode (encode, bin, &length) < 0)
		CF_DEBUG_PRINT (stderr, "error\n");
	else
		CF_DEBUG_PRINT_BIN (stderr, bin, length, "bin    : %s\n", bin);

	memset (bin   , 0x00, sizeof (bin));
	memset (encode, 0x00, sizeof (encode));
	length = 0;

	/* base64 */
	CF_DEBUG_PRINT (stderr, "----------------- codec/base64 ---------------\n");
	CF_DEBUG_PRINT (stderr, "data   : %s\n", data);
	CF_DEBUG_PRINT (stderr, "= Convert binary to base64 =\n");
	CF_Codec_Base64_Encode ((unsigned char *)data, strlen (data), encode);
	CF_DEBUG_PRINT (stderr, "base64 : %s\n", encode);

	CF_DEBUG_PRINT (stderr, "= Convert base64 to binary =\n");
	if (CF_Codec_Base64_Decode (encode, bin, &length) < 0) {
		// error
	}
	else
		CF_DEBUG_PRINT_BIN (stderr, bin, length, "bin    : %s\n", bin);

	return 0;
}
