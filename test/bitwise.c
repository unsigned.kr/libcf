/**
 * @file	bitwise.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_bitwise.h"

#include <stdio.h>
#include <string.h>

#ifdef _WIN32
# include <winsock2.h>
# pragma comment(lib, "ws2_32.lib")
#else
# include <arpa/inet.h>
#endif

int main (void)
{
	cf_byte			in[sizeof (int)] = {0x00,};
	cf_byte			out[sizeof (int)] = {0x00,};
	unsigned int	integer = 0x6b8b4567;		// random byte
	unsigned int	* bi = (unsigned int *)in;	// type punning
	unsigned int	* bo = (unsigned int *)out;	// type punning

	// print original
	printf ("data           : 0x%08x\n", integer);
	printf ("---------------------------\n");

	// if in little-endian environment,
	// convert integer to big-endian for pure bit-array
	*bi = htonl (integer);
	CF_Bitwise_ShiftLeft (in, sizeof (int) * 8, 4, out);

	// print result of operation
	printf ("integer   << 4 : 0x%08x\n", integer << 4);
	printf ("byteArray << 4 : 0x%08x\n", ntohl (*bo));

	return 0;
}
