/**
 * @file	thread.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_thread.h"
#include "cf_mutex.h"

#include <stdio.h>

#define	COUNT	5

cf_ctx globalMutex;

int worker (void * arg)
{
	if (CF_Mutex_Lock (globalMutex) < 0) { // for critical section
		// error
	}

	fprintf (stderr, "here is critical section !\n");

	if (CF_Mutex_Unlock (globalMutex) < 0) { // for critical section
		// error
	}

	return 0;
}

int main (void)
{
	cf_ctx	tid[COUNT];
	int		i = 0;

	if (CF_Mutex_Create (&globalMutex) < 0) {
		// error
	}

	for (i = 0 ; i < COUNT ; i++) {
		if (CF_Thread_Create (&tid[i], worker, NULL) < 0) {
			// error
		}
	}

	for (i = 0 ; i < COUNT ; i++) {
		if (CF_Thread_Start (tid[i]) < 0) {
			// error
		}
	}

	for (i = 0 ; i < COUNT ; i++) {
		if (CF_Thread_Join (tid[i]) < 0) { // block
			// error
		}
	}

	for (i = 0 ; i < COUNT ; i++) {
		if (CF_Thread_Destroy (tid[i]) < 0) {
			// error
		}
	}

	if (CF_Mutex_Destory (globalMutex) < 0) {
		// error
	}
	return 0;
}
