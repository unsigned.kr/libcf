/**
 * @file	queue.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_queue.h"
#include "cf_debug.h"

#define	COUNT	5

int main (void)
{
	long long int	iter = 0;
	int				result = 0;
	long long int	element = 0;
	cf_ctx			queue = NULL;

	// create
	result = CF_Queue_Create (&queue);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	// insert
	for (iter = 0 ; iter < COUNT ; iter++)
	{
		result = CF_Queue_Put (queue, (void *)iter);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);
	}

	// move next
	for (iter = 0 ; iter < COUNT ; iter++)
	{
		result = CF_Queue_Front (queue, (void **)&element);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);
		else
			CF_DEBUG_PRINT (stderr, "front : %4d\n", element);

		result = CF_Queue_Get (queue, (void **)&element);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);
		else
			CF_DEBUG_PRINT (stderr, "got   : %4d\n", element);

		CF_DEBUG_PRINT (stderr, "\n");
	}

	// destroy
	result = CF_Queue_Destroy (queue);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	return 0;
}
