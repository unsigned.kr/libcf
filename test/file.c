/**
 * @file	file.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_file.h"
#include <stdio.h>

int main (void)
{
	cf_ctx fd = 0;
	int result = 0;
	char *dir1 = "./dir1/test";
	char *dir2 = "dir2/test";
	char *name = "file.txt";
	char buffer[128] = {0x00,};

	if (CF_File_MakeDirectory (dir1)) {
		// error
	}

	if (CF_File_MakeDirectory (dir2)) {
		// error
	}

	result = CF_File_Open (&fd, name, CF_FILE_CREATE|CF_FILE_RW|CF_FILE_TRUNC);
	if (result < 0) {
		// error
	}
	if (CF_File_Write (fd, "file test", 9) < 0) {
		// error
	}
	CF_File_Close (fd);

	result = CF_File_Open (&fd, name, CF_FILE_READ);
	if (result < 0) {
		// error
	}
	CF_File_GetPath (fd, buffer);
	printf ("file path : %s\n", buffer);
	printf ("file size : %d\n", CF_File_GetSize (name));
	if (CF_File_Read (fd, buffer, sizeof (buffer)) < 0) {
		// error
	}
	CF_File_Close (fd);

	return 0;
}
