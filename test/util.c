/**
 * @file	util.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_util.h"
#include <stdio.h>

int main (void)
{
	CF_UTIL_DATETIME dt;

	char buf[CF_UTIL_DATETIME_LENGTH + 1] = {0x00,};

	CF_Util_GetCurrentTime (&dt);

	CF_Util_GetTimeString (&dt, buf);

	printf ("current time : %s\n", buf);

	return 0;
}
