/**
 * @file	list.c
 * @author	myusgun <myusgun@gmail.com>
 */
#include "cf_list.h"
#include "cf_debug.h"

int main (void)
{
	long long int	iter = 0;
	int				result = 0;
	long long int	element = 0;
	cf_ctx			list = NULL;
	cf_traverser	trav = NULL;

	result = CF_List_Create (&list);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	for (iter = 0 ; iter < 10 ; iter++)
	{
		result = CF_List_InsertBefore (list, trav, (void *)iter);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);
	}

	result = CF_List_Front (list, &trav);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	for (iter = 0 ; iter < 5 ; iter++)
	{
		result = CF_List_Next (&trav);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);
	}

	result = CF_List_InsertBefore (list, trav, (void *)100);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	result = CF_List_InsertAfter (list, trav, (void *)200);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	for (CF_List_Front (list, &trav) ; trav != NULL ; CF_List_Next (&trav))
	{
		result = CF_List_Get (trav, (void **)&element);
		if (result < 0)
			CF_DEBUG_PRINT (stderr, "error %d\n", result);

		CF_DEBUG_PRINT (stderr, "%d \n", element);
	}

	result = CF_List_Destroy (list);
	if (result < 0)
		CF_DEBUG_PRINT (stderr, "error %d\n", result);

	return 0;
}
