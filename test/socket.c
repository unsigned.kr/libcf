#include "cf_socket.h"
#include "cf_debug.h"
#include "cf_log.h"
#include "cf_thread.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>

#define THREAD_POOL		2
#define PORT			1234
#define COUNT			2

cf_ctx gLog;

int server_worker (void * arg)
{
	cf_ctx srvsock = (cf_ctx)arg;
	cf_ctx clntsock = NULL;
	int result = 0;
	int recvd = 0;
	char buf[1024] = {0x00,};
	int i = 0;

	/*------------------------------------------------------------*/
	result = CF_Socket_Accept (srvsock, &clntsock);
	if (result < 0)
	{
		CF_DEBUG_PRINT (stderr, "failed to accept on server\n");
		return -1;
	}
	CF_Log_Write (gLog, "SERVER", "accepted\n");

	for (i = 0 ; i < COUNT ; i++)
	{
		if ((recvd = CF_Socket_Recv (clntsock, buf, sizeof (buf))) < 0)
		{
			CF_DEBUG_PRINT (stderr, "failed to recv on server\n");
			return -2;
		}
		CF_Log_Write (gLog, "SERVER", "recv {%s}\n", buf);
		CF_DEBUG_PRINT (stderr, "recv {%s}\n", buf);

		if (CF_Socket_Send (clntsock, buf, (size_t)recvd) < 0)
		{
			return -3;
		}
		CF_Log_Write (gLog, "SERVER", "resp {%s}\n", buf);
		CF_DEBUG_PRINT (stderr, "resp {%s}\n", buf);
	}

	CF_Socket_Close (clntsock);

	return 0;
	/*------------------------------------------------------------*/
}

int client_worker (void * arg)
{
	cf_ctx sock = NULL;
	int result = 0;
	int recvd = 0;
	char buf[1024] = {0x00,};
	int i = 0;

	sprintf (buf, "...wow ? is it succeed ?");

	/*------------------------------------------------------------*/
	result = CF_Socket_Create (&sock);
	if (result < 0)
	{
		CF_DEBUG_PRINT (stderr, "failed to create\n");
		return -1;
	}

	result = CF_Socket_Connect (sock, "localhost", PORT);
	if (result < 0)
	{
		CF_DEBUG_PRINT (stderr, "failed to connect on client\n");
		return -2;
	}
	CF_Log_Write (gLog, "CLIENT", "connected\n");

	for (i = 0 ; i < COUNT ; i++)
	{
		if (CF_Socket_Send (sock, buf, sizeof (buf)) < 0)
		{
			CF_DEBUG_PRINT (stderr, "failed to send on client %d\n", errno);
			return -3;
		}
		CF_Log_Write (gLog, "CLIENT", "sent {%s}\n", buf);
		CF_DEBUG_PRINT (stderr, "sent {%s}\n", buf);

		memset (buf, 0x00, sizeof (buf));

		if ((recvd = CF_Socket_Recv (sock, buf, sizeof (buf))) < 0)
		{
			CF_DEBUG_PRINT (stderr, "failed to recv on client\n");
			return -4;
		}
		CF_Log_Write (gLog, "CLIENT", "recv {%s}\n", buf);
		CF_DEBUG_PRINT (stderr, "recv {%s}\n", buf);
	}

	CF_Socket_Close (sock);

	return 0;
	/*------------------------------------------------------------*/
}

int main ()
{
	cf_ctx stid[THREAD_POOL];
	cf_ctx ctid[THREAD_POOL];

	char ip[256] = {0x00,};
	unsigned short port = 0;
	cf_ctx sock = 0;
	int result = 0;
	int iter = 0;

	/*------------------------------------------------------------*/
	if (CF_Log_Create (&gLog, "socket.txt", CF_LOG_DEFAULT_BUFFER) < 0)
		CF_DEBUG_PRINT (stderr, "failed to open log\n");

	CF_Log_Write (gLog, "LOG_MULTITHREAD", "multi-threaded logging test with socket\n");

	if (CF_Socket_Initialize () < 0)
	{
		CF_DEBUG_PRINT (stderr, "failed to initialize socket\n");
		return -1;
	}
	CF_Log_Write (gLog, "SOCKET", "socket initialized\n");

	result = CF_Socket_Create (&sock);
	if (result < 0)
	{
		CF_DEBUG_PRINT (stderr, "failed to create\n");
		return -2;
	}

	result = CF_Socket_Server (sock, PORT, 5);
	if (sock < 0)
	{
		CF_DEBUG_PRINT (stderr, "failed to ready server %d\n", sock);
		return -3;
	}
	CF_Socket_GetIP (sock, ip);
	CF_Socket_GetPort (sock, &port);
	CF_Log_Write (gLog, "SOCKET", "socket ready [%s:%d]\n", ip, port);
	CF_DEBUG_PRINT (stderr, "socket ready [%s:%d]\n", ip, port);
 

	for (iter = 0 ; iter < THREAD_POOL ; iter++)
	{
		if (CF_Thread_Create (&stid[iter], server_worker, sock) < 0)
		{
			CF_DEBUG_PRINT (stderr, "failed to create %dth thread\n", iter);
			return -4;
		}
		CF_Log_Write (gLog, "SOCKET", "create server thread-%d\n", iter);
	}

	for (iter = 0 ; iter < THREAD_POOL ; iter++)
	{
		if (CF_Thread_Create (&ctid[iter], client_worker, &sock) < 0)
		{
			CF_DEBUG_PRINT (stderr, "failed to create %dth thread\n", iter);
			return -5;
		}
		CF_Log_Write (gLog, "SOCKET", "create client thread-%d\n", iter);
	}

	for (iter = 0 ; iter < THREAD_POOL ; iter++)
	{
		if (CF_Thread_Start (stid[iter]) < 0)
		{
			CF_DEBUG_PRINT (stderr, "failed to start %dth thread\n", iter);
			return -6;
		}
	}

	for (iter = 0 ; iter < THREAD_POOL ; iter++)
	{
		if (CF_Thread_Start (ctid[iter]) < 0)
		{
			CF_DEBUG_PRINT (stderr, "failed to start %dth thread\n", iter);
			return -7;
		}
	}

	for (iter = 0 ; iter < THREAD_POOL ; iter++)
	{
		CF_Thread_Join (ctid[iter]);
		CF_Thread_Destroy (ctid[iter]);

		CF_Thread_Join (stid[iter]);
		CF_Thread_Destroy (stid[iter]);

		CF_Log_Write (gLog, "SOCKET", "join server thread-%d\n", iter);
	}

	CF_Socket_Close (sock);

	CF_Socket_Finalize ();

	CF_Log_Destroy (gLog);
	/*------------------------------------------------------------*/

	return 0;
}
