#------------------------------------------------------------
# set command to check os
#------------------------------------------------------------
UNAME_S := $(shell uname -s)
UNAME_M := $(shell uname -m)
UNAME_R := $(shell uname -r)
UNAME_V := $(shell uname -v)
UNAME_A := $(shell uname -a)

#------------------------------------------------------------
# set options
#------------------------------------------------------------
ifeq ($(DEBUG), yes)
	DEFS = -D_DEBUG
	FLAG = -g
else
	DEFS = 
	FLAG = -O2
endif

EXT_SHARED	= so
EXT_STATIC	= a
EXT_EXECUTE	= ex

#------------------------------------------------------------
# set platform
#------------------------------------------------------------
ifeq ($(UNAME_S), SunOS)
	ifeq ($(UNAME_M), i86pc)
		ifeq ($(VER), 32)
			OS_DEF = solaris_x86
		else
			OS_DEF = solaris_x86_64
		endif
	else
		ifeq ($(VER), 32)
			OS_DEF = solaris
		else
			OS_DEF = solaris64
		endif
	endif
endif

ifeq ($(UNAME_S), AIX)
	ifeq ($(VER), 32)
		OS_DEF = aix
	else
		OS_DEF = aix64
	endif
endif

ifeq ($(UNAME_S), HP-UX)
	ifeq ($(UNAME_M), ia64)
		UNAME_S := HP-UX_IA
		ifeq ($(VER), 32)
			OS_DEF = hp_ia
		else
			OS_DEF = hp_ia64
		endif
	else
		ifeq ($(VER), 32)
			OS_DEF = hp
		else
			OS_DEF = hp64
		endif
	endif
endif

ifeq ($(UNAME_S), Linux)
	ifeq ($(UNAME_M), ppc64)
		OS_DEF = linux_ppc64
	else
		ifeq ($(VER), 32)
			OS_DEF = linux
		else
			OS_DEF = linux_x86_64
		endif
	endif
endif

ifeq ($(UNAME_S), Darwin)
	ifeq ($(VER), 32)
		OS_DEF = mac
	else
		OS_DEF = mac_x86_64
	endif
endif

ifeq ($(filter $(UNAME_A),Cygwin),Cygwin)
	# forced
	VER			= 32
	EXT_SHARED	= dll
	EXT_STATIC	= lib
	EXT_EXECUTE	= exe

	# run as linux
	OS_DEF		= linux
endif

#------------------------------------------------------------
# set build env.
#------------------------------------------------------------
ifeq ($(OS_DEF), $(filter $(OS_DEF),solaris solaris64 solaris_x86 solaris_x86_64))
CC				= gcc
LD				= gcc
AR				= ar
CDEFS			= $(DEFS) -D_SOLARIS -D_REENTRANT
CFLAGS			= $(FLAG) -m$(VER) -Wall -Wconversion -Wpointer-arith -Wcast-align -fPIC
LDFLAGS			= -m$(VER)
SHARED_FLAGS	= $(LDFLAGS) -G
ARFLAGS			= rc
LIBS			= -lnsl -lsocket -lpthread
endif

ifeq ($(OS_DEF), $(filter $(OS_DEF),aix aix64))
CC				= cc_r
LD				= ld
AR				= ar
CDEFS			= $(DEFS) -D_AIX -D_REENTRANT -D__STDC__=1
CFLAGS			= $(FLAG) -q$(VER) -qcpluscmt
LDFLAGS			= -b$(VER) -brtl
SHARED_FLAGS	= $(LDFLAGS) -bnoentry -bM:SRE -bexpall
ARFLAGS			= cru
LIBS			= -lc -lpthread
endif

ifeq ($(OS_DEF), $(filter $(OS_DEF),hp hp64 hp_ia hp_ia64))
CC				= cc
LD				= ld
AR				= ar
CDEFS			= $(DEFS) -D_HPUX -D_REENTRANT
CFLAGS			= $(FLAG) +z -q -n
LDFLAGS			= 
SHARED_FLAGS	= $(LDFLAGS) -b +s
ARFLAGS			= cru
LIBS			= -lc -lpthread

	ifeq ($(OS_DEF), hp)
		CFLAGS		+= +DA1.1 +s
		EXT_SHARED	= sl
	endif

	ifeq ($(OS_DEF), hp64)
		CFLAGS		+= +DA2.0W +w1 -Ae -z +u4
		EXT_SHARED	= sl
	endif

	ifeq ($(OS_DEF), hp_ia)
		CFLAGS		+= +DA1.1 +s
	endif

	ifeq ($(OS_DEF), hp_ia64)
		CFLAGS	+= +DD64 +w1 -Ae -z +u4
	endif
endif

ifeq ($(OS_DEF), $(filter $(OS_DEF),linux linux_x86_64 linux_ppc64))
CC				= gcc
LD				= gcc
AR				= ar
CDEFS			= $(DEFS) -D_LINUX -D_REENTRANT
CFLAGS			= $(FLAG) -m$(VER) -Wall -Wconversion -Wpointer-arith -Wcast-align -fPIC
LDFLAGS			= -m$(VER)
SHARED_FLAGS	= $(LDFLAGS) -shared
ARFLAGS			= rc
LIBS			= -lpthread
endif

ifeq ($(OS_DEF), $(filter $(OS_DEF),mac mac_x86_64))
CC				= gcc
LD				= gcc
AR				= ar
CDEFS			= $(DEFS) -D_MAC -D_REENTRANT
CFLAGS			= $(FLAG) -m$(VER) -Wall -Wconversion -Wpointer-arith -Wcast-align -fPIC
LDFLAGS			= -m$(VER)
SHARED_FLAGS	= $(LDFLAGS) -shared
ARFLAGS			= rc
LIBS			= -lpthread
endif
