#------------------------------------------------------------
# user definition
#------------------------------------------------------------

# os-version
# > 32 or 64
VER				= 64

# compile with debugging flag and definition
# > YES or NO
DEBUG			= yes

# create doxygen documents automatically
# > YES or NO
DOXYGEN_CREATE	= yes
