/**
 * \file cf_codec.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 데이터 변환
 *
 * \example codec.c
 */
#ifndef __CF_CODEC_H__
#define __CF_CODEC_H__

#include "cf_base.h"
#include "cf_type.h"

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Codec_Hex_Encode			(const cf_byte	* bin,
							 const size_t	len,
							 char			* hex);

CF_EXPORT int
CF_Codec_Hex_Decode			(const char	* hex,
							 cf_byte	* bin,
							 size_t		* len);

CF_EXPORT int
CF_Codec_Base64_Encode		(const cf_byte	* bin,
							 const size_t	len,
							 char			* base64);

CF_EXPORT int
CF_Codec_Base64_Decode		(const char	* base64,
							 cf_byte	* bin,
							 size_t		* len);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_CODEC_H__
