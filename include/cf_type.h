/**
 * \file cf_type.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 자료형 정의
 */
#ifndef __CF_TYPE_H__
#define __CF_TYPE_H__

typedef		unsigned char		cf_byte;
typedef		unsigned short		cf_word;
typedef		unsigned int		cf_dword;

#endif /// #ifndef __CF_TYPE_H__
