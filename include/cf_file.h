/**
 * \file cf_file.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 파일 입출력
 *
 * \example file.c
 */
#ifndef __CF_FILE_H__
#define __CF_FILE_H__

#include "cf_base.h"

#include <stddef.h>

/** 파일 열기 옵션 플래그 */
typedef enum
{
	CF_FILE_READ		= 0x0001,	/**< 읽기 전용 */
	CF_FILE_WRITE		= 0x0002,	/**< 쓰기 전용 */
	CF_FILE_RW			= 0x0004,	/**< 읽기/쓰기 */
	CF_FILE_CREATE		= 0x0008,	/**< 파일이 존재하지 않으면 생성 */
	CF_FILE_TRUNC		= 0x0010,	/**< 파일이 존재하면 비우기 */
	CF_FILE_APPEND		= 0x0020	/**< 파일이 존재하면 이어서 쓰기 */
} CF_FILE_FLAG;

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_File_Open			(cf_ctx				* ctx,
						 const char			* path,
						 const CF_FILE_FLAG	flag);

CF_EXPORT int
CF_File_Close			(cf_ctx ctx);

CF_EXPORT int
CF_File_Read			(const cf_ctx	ctx,
						 void			* buf,
						 const size_t	len);

CF_EXPORT int
CF_File_Write			(const cf_ctx	ctx,
						 const void		* buf, 
						 const size_t	len);

CF_EXPORT int
CF_File_GetSize			(const char * path);

CF_EXPORT CF_BOOL
CF_File_Exists			(const char * path);

CF_EXPORT int
CF_File_MakeDirectory	(const char * path);

CF_EXPORT int
CF_File_GetPath			(const cf_ctx	ctx,
						 char			* path);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_FILE_H__
