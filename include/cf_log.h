/**
 * \file cf_log.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 로그
 *
 * \remarks
 * 매핑 ID 값을 이용하여 해당 로그에 기록 <br />
 * 로그 레벨을 적용할 때도 용이하며 define 이나 enumeration 등을 활용하면 좋음
 *
 * \example log.c
 */
#ifndef __CF_LOG_H__
#define __CF_LOG_H__

#include "cf_base.h"

/** 로그 옵션 플래그 */
typedef enum
{
	CF_LOG_DEFAULT_BUFFER	= -1,	/**< 기본 버퍼 크기 사용 */
	CF_LOG_NO_BUFFER		= 0,	/**< 버퍼없이 파일에 바로 쓰기 */
} CF_LOG_OPTION;

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Log_Create			(cf_ctx		* ctx,
						 const char	* path,
						 const int	memsize);

CF_EXPORT int
CF_Log_Destroy			(cf_ctx ctx);

CF_EXPORT int
CF_Log_Write			(cf_ctx		ctx,
						 const char	* prefix,
						 const char	* fmt, ...);

CF_EXPORT int
CF_Log_Flush			(cf_ctx ctx);

CF_EXPORT int
CF_Log_SetMultiThread	(cf_ctx ctx);

CF_EXPORT int
CF_Log_UnsetMultiThread	(cf_ctx ctx);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_LOG_H__
