/**
 * \file cf_thread.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 멀티스레드 지원
 *
 * \example thread.c
 */
#ifndef __CF_THREAD_H__
#define __CF_THREAD_H__

#include "cf_base.h"

/** 스레드 워커 함수 프로토타입 */
typedef int (* CF_Thread_Function) (void *);

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Thread_Create		(cf_ctx				* ctx,
						 CF_Thread_Function	callback,
						 void				* arg);

CF_EXPORT int
CF_Thread_Start			(cf_ctx ctx);

CF_EXPORT int
CF_Thread_Destroy		(cf_ctx ctx);

CF_EXPORT int
CF_Thread_Join			(cf_ctx ctx);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_THREAD_H__
