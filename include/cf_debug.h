/**
 * \file cf_debug.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 디버그 유틸
 *
 * \remarks
 * 디버그 함수를 직접 호출하지 않고 정의된 매크로를 사용하면,
 * Preprocessor에 _DEBUG가 정의되어 있는 경우에 만
 * 디버그 코드가 동작하도록 할 수 있음 <br />
 * 디버그 메시지는 stdout 및 stderr 등의 파일 포인터 출력을 지원함
 *
 * \example debug.c
 */
#ifndef __CF_DEBUG_H__
#define __CF_DEBUG_H__

#include "cf_base.h"
#include "cf_type.h"

#include <stdio.h>
#include <stddef.h>

/** Windows 함수 이름 매크로 재정의 */
#if defined(_WIN32) || defined(_WIN64)
# define __func__	__FUNCTION__
#endif

#ifdef _DEBUG

/**
 * 디버그 메시지를 지정된 파일 포인터로 출력
 *
 * \param __fp	출력할 파일 포인터
 * \param __fmt	포맷 스트링
 * \param ...	가변 인자
 */
# define CF_DEBUG_PRINT(__fp,__fmt,...)	\
	CF_Debug_Print (__fp,__FILE__,__func__,__LINE__,__fmt,##__VA_ARGS__)

/**
 * 바이너리 데이터를 디버그 메시지와 함께 지정된 파일 포인터로 출력
 *
 * \param __fp	출력할 파일 포인터
 * \param __bin	바이너리 데이터
 * \param __len	바이너리 길이
 * \param __fmt	포맷 스트링
 * \param ...	가변 인자
 */
# define CF_DEBUG_PRINT_BIN(__fp,__bin,__len,__fmt,...)	\
	CF_Debug_PrintBin (__fp,__FILE__,__func__,__LINE__,__bin,__len,__fmt,##__VA_ARGS__)

/** 디버깅 모듈 초기화 */
# define CF_DEBUG_INITIALIZE	\
	CF_Debug_Initialize ()

/** 디버깅 모듈 해제 */
# define CF_DEBUG_FINALIZE	\
	CF_Debug_Finalize ()

/** 함수에 진입 */
# define CF_DEBUG_BEGIN_FUNCTION	\
	CF_Debug_EnterFunction (__FILE__,__func__,__LINE__)

/** 함수에서 리턴 */
# define CF_DEBUG_END_FUNCTION	\
	CF_Debug_LeaveFunction ()

/** 콜스택 을 지정된 파일 포인터로 출력 */
# define CF_DEBUG_PRINT_CALLSTACK(__fp)	\
	CF_Debug_PrintCallStack (__fp)

#else // #ifdef _DEBUG
# define CF_DEBUG_PRINT(__fp,__fmt,...)
# define CF_DEBUG_PRINT_BIN(__fp,__bin,__len,__fmt,...)
# define CF_DEBUG_INITIALIZE
# define CF_DEBUG_FINALIZE
# define CF_DEBUG_BEGIN_FUNCTION
# define CF_DEBUG_END_FUNCTION
# define CF_DEBUG_PRINT_CALLSTACK(__fp)
#endif // #ifdef _DEBUG

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Debug_Print			(FILE		* fp,
						 const char	* file,
						 const char	* func,
						 const int	line,
						 const char	* fmt, ...);

CF_EXPORT int
CF_Debug_PrintBin		(FILE			* fp,
						 const char		* file,
						 const char		* func,
						 const int		line,
						 const cf_byte	* bin,
						 const size_t	len,
						 const char		* fmt, ...);

CF_EXPORT int
CF_Debug_Initialize		(void);

CF_EXPORT int
CF_Debug_Finalize		(void);

CF_EXPORT int
CF_Debug_PrintCallStack	(FILE * fp);

CF_EXPORT int
CF_Debug_EnterFunction	(const char	* file,
						 const char	* func,
						 const int	line);

CF_EXPORT int
CF_Debug_LeaveFunction	(void);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_DEBUG_H__
