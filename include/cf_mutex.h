/**
 * \file cf_mutex.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 뮤텍스 지원
 */
#ifndef __CF_MUTEX_H__
#define __CF_MUTEX_H__

#include "cf_base.h"

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Mutex_Create			(cf_ctx * ctx);

CF_EXPORT int
CF_Mutex_Destory		(cf_ctx ctx);

CF_EXPORT int
CF_Mutex_Lock			(cf_ctx ctx);

CF_EXPORT int
CF_Mutex_Unlock			(cf_ctx ctx);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_MUTEX_H__

