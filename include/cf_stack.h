/**
 * \file cf_stack.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 스택
 *
 * \example stack.c
 */
#ifndef __CF_STACK_H__
#define __CF_STACK_H__

#include "cf_base.h"

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Stack_Create			(cf_ctx * ctx);

CF_EXPORT int
CF_Stack_Destroy		(cf_ctx ctx);

CF_EXPORT int
CF_Stack_Push			(cf_ctx		ctx,
						 const void	* element);

CF_EXPORT int
CF_Stack_Pop			(cf_ctx	ctx,
						 void	** element);

CF_EXPORT int
CF_Stack_Top			(cf_ctx	ctx,
						 void	** element);

CF_EXPORT int
CF_Stack_GetSize		(cf_ctx ctx);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_STACK_H__
