/**
 * \file cf_util.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 유틸
 *
 * \example util.c
 */
#ifndef __CF_UTIL_H__
#define __CF_UTIL_H__

#include "cf_base.h"

#define CF_UTIL_DATETIME_LENGTH	(sizeof ("0000-00-00 00:00:00.000") - 1)

typedef enum 
{
	SUN = 0,	/**< 일요일 */
	MON,		/**< 월요일 */
	TUE,		/**< 화요일 */
	WED,		/**< 수요일 */
	THU,		/**< 목요일 */
	FRI,		/**< 금요일 */
	SAT,		/**< 토요일 */
} CF_UTIL_WEEK;

typedef struct __cf_util_datetime__
{
	int				year;	/**< 년. */
	int				month;	/**< 월. */
	int				day;	/**< 일. */
	CF_UTIL_WEEK	week;	/**< 요일. */

	int				hour;	/**< 시간. */
	int				min;	/**< 분. */
	int				sec;	/**< 초. */
	int				usec;	/**< 마이크로 초. */
} CF_UTIL_DATETIME;

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Util_GetSystemError		(void);

CF_EXPORT int
CF_Util_GetCurrentTime		(CF_UTIL_DATETIME * dt);

CF_EXPORT int
CF_Util_GetTimeString		(const CF_UTIL_DATETIME	* dt,
							 char					* str);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_UTIL_H__
