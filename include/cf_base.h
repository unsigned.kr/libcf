/**
 * \file cf_base.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \version	0.1
 *
 * \brief 기본 정의
 *
 * \mainpage

<h2 style="line-height: 10%;">Welcome to documentation for libcf</h2>
&nbsp; :: CFLibrary (Cheese Foundation Library)

&nbsp; This documentation (suggested by nvdark) includes only manual of APIs.

&nbsp; Please visit <a href="/trac/libcf">Trac</a> of libcf.

<h3>supported functions</h3>
<ol>
 <li> File I/O </li>
 <li> Socket </li>
 <li> Logging (advisor: vfire) </li>
 <li> Utilities for Debug </li>
 <li> Thread & Mutex </li>
 <li> Codec </li>
 <li> Data Structure </li>
 <li> ..more needs ? </li>
</ol>

*/
#ifndef __CF_BASE_H__
#define __CF_BASE_H__

/** boolean 타입 */
typedef enum
{
	CF_FALSE = 0,	/**< 거짓 */
	CF_TRUE = 1		/**< 참 */
} CF_BOOL;

/** 공용 건텍스트 */
typedef void *	cf_ctx;

/** 성공 시, 반환 값 */
#define CF_OK							0
/** 오류 코드 */
#define CF_ERROR						-1

/** Windows 에서의 함수 EXPORT 구문 정의 */
#if defined(_WIN32) || defined(_WIN64)
# define CF_EXPORT		__declspec(dllexport)
#else
# define CF_EXPORT
#endif

#endif // #ifndef __CF_BASE_H__
