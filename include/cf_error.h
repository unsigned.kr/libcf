/**
 * \file cf_error.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 오류코드 모음
 */
#ifndef __CF_ERROR_H__
#define __CF_ERROR_H__

#include "cf_base.h"

/** 오류 코드 베이스 넘버 */
#define CF_ERROR_BASE						CF_ERROR * 1000

/* file {{{ */
#define CF_ERROR_FILE						CF_ERROR_BASE * 1
/*------------------------------------------------------------*/
#define CF_ERROR_FILE_OPEN					CF_ERROR_FILE - 1
#define CF_ERROR_FILE_INVALID_ARGS			CF_ERROR_FILE - 2
#define CF_ERROR_FILE_READ					CF_ERROR_FILE - 3
#define CF_ERROR_FILE_WRITE					CF_ERROR_FILE - 4
#define CF_ERROR_FILE_CREATE_CTX			CF_ERROR_FILE - 5
#define CF_ERROR_FILE_CLOSE					CF_ERROR_FILE - 6
#define CF_ERROR_FILE_GET_SIZE				CF_ERROR_FILE - 7
#define CF_ERROR_FILE_MAKE_DIRECTORY		CF_ERROR_FILE - 8
#define CF_ERROR_FILE_ALLOCATE_BUFFER		CF_ERROR_FILE - 9
#define CF_ERROR_FILE_INVALID_CTX			CF_ERROR_FILE - 10
/* }}} file */

/* socket {{{ */
#define CF_ERROR_SOCKET						CF_ERROR_BASE * 2
/*------------------------------------------------------------*/
#define CF_ERROR_SOCKET_INITIALIZE			CF_ERROR_SOCKET - 1
#define CF_ERROR_SOCKET_NOT_INITIALIZED		CF_ERROR_SOCKET - 2
#define CF_ERROR_SOCKET_FINALIZE			CF_ERROR_SOCKET - 3
#define CF_ERROR_SOCKET_CLOSE				CF_ERROR_SOCKET - 4
#define CF_ERROR_SOCKET_CREATE				CF_ERROR_SOCKET - 5
#define CF_ERROR_SOCKET_CONNECT				CF_ERROR_SOCKET - 6
#define CF_ERROR_SOCKET_SET_OPTION			CF_ERROR_SOCKET - 7
#define CF_ERROR_SOCKET_GET_OPTION			CF_ERROR_SOCKET - 8
#define CF_ERROR_SOCKET_TIMEOUT				CF_ERROR_SOCKET - 9
#define CF_ERROR_SOCKET_INVALID_ARGS		CF_ERROR_SOCKET - 10
#define CF_ERROR_SOCKET_INVALID_SOCKET		CF_ERROR_SOCKET - 11
#define CF_ERROR_SOCKET_GET_HOST			CF_ERROR_SOCKET - 12
#define CF_ERROR_SOCKET_BIND				CF_ERROR_SOCKET - 13
#define CF_ERROR_SOCKET_LISTEN				CF_ERROR_SOCKET - 14
#define CF_ERROR_SOCKET_ACCEPT				CF_ERROR_SOCKET - 15
#define CF_ERROR_SOCKET_SEND				CF_ERROR_SOCKET - 16
#define CF_ERROR_SOCKET_RECV				CF_ERROR_SOCKET - 17
#define CF_ERROR_SOCKET_CHECK_DESC_SET		CF_ERROR_SOCKET - 18
#define CF_ERROR_SOCKET_INVALID_CTX			CF_ERROR_SOCKET - 19
#define CF_ERROR_SOCKET_CREATE_CTX			CF_ERROR_SOCKET - 20
/* }}} socket */

/* thread {{{ */
#define CF_ERROR_THREAD						CF_ERROR_BASE * 3
/*------------------------------------------------------------*/
#define CF_ERROR_THREAD_CREATE_CTX			CF_ERROR_THREAD - 1
#define CF_ERROR_THREAD_START				CF_ERROR_THREAD - 2
#define CF_ERROR_THREAD_INVALID_CTX			CF_ERROR_THREAD - 3
#define CF_ERROR_THREAD_INVALID_ARGS		CF_ERROR_THREAD - 4
#define CF_ERROR_THREAD_INIT_ATTR			CF_ERROR_THREAD - 5
#define CF_ERROR_THREAD_SET_INHERIT_SCHED	CF_ERROR_THREAD - 6
#define CF_ERROR_THREAD_SET_SCHED_POLICY	CF_ERROR_THREAD - 7
#define CF_ERROR_THREAD_SET_SCHED_PARAM		CF_ERROR_THREAD - 8
/* }}} thread */

/* mutex {{{ */
#define CF_ERROR_MUTEX						CF_ERROR_BASE * 4
/*------------------------------------------------------------*/
#define CF_ERROR_MUTEX_CREATE_CTX			CF_ERROR_MUTEX - 1
#define CF_ERROR_MUTEX_CREATE				CF_ERROR_MUTEX - 2
#define CF_ERROR_MUTEX_INVALID_CTX			CF_ERROR_MUTEX - 3
#define CF_ERROR_MUTEX_INVALID_ARGS			CF_ERROR_MUTEX - 4
/* }}} mutex */

/* debug {{{ */
#define CF_ERROR_DEBUG						CF_ERROR_BASE * 5
/*------------------------------------------------------------*/
#define CF_ERROR_DEBUG_INVALID_CTX			CF_ERROR_DEBUG - 1
#define CF_ERROR_DEBUG_PUSH_CALLSTACK		CF_ERROR_DEBUG - 2
#define CF_ERROR_DEBUG_POP_CALLSTACK		CF_ERROR_DEBUG - 3
#define CF_ERROR_DEBUG_PEEK_CALLSTACK		CF_ERROR_DEBUG - 4
#define CF_ERROR_DEBUG_CREATE_CTX			CF_ERROR_DEBUG - 5
/* }}} debug */

/* log {{{ */
#define CF_ERROR_LOG						CF_ERROR_BASE * 6
/*------------------------------------------------------------*/
#define CF_ERROR_LOG_INVALID_CTX			CF_ERROR_LOG - 1
#define CF_ERROR_LOG_SET_MULTITHREAD		CF_ERROR_LOG - 2
#define CF_ERROR_LOG_UNSET_MULTITHREAD		CF_ERROR_LOG - 3
#define CF_ERROR_LOG_FLUSH					CF_ERROR_LOG - 4
#define CF_ERROR_LOG_INVALID_ARGS			CF_ERROR_LOG - 5
#define CF_ERROR_LOG_CREATE_CTX				CF_ERROR_LOG - 6
#define CF_ERROR_LOG_CREATE_FILE			CF_ERROR_LOG - 7
#define CF_ERROR_LOG_ALLOCATE_BUFFER		CF_ERROR_LOG - 8
/* }}} log */

/* codec {{{ */
#define CF_ERROR_CODEC						CF_ERROR_BASE * 7
/*------------------------------------------------------------*/
#define CF_ERROR_CODEC_INVALID_ARGS			CF_ERROR_CODEC - 1
#define CF_ERROR_CODEC_NOT_HEXSTRING		CF_ERROR_CODEC - 2
#define CF_ERROR_CODEC_NOT_BASE64			CF_ERROR_CODEC - 3
/* }}} codec */

/* data structure - list/queue/stack {{{ */
#define CF_ERROR_DS							CF_ERROR_BASE * 8
/*------------------------------------------------------------*/
#define CF_ERROR_DS_INVALID_CTX				CF_ERROR_DS - 1
#define CF_ERROR_DS_INVALID_TRAVERSER		CF_ERROR_DS - 2
#define CF_ERROR_DS_INVALID_ARGS			CF_ERROR_DS - 3
#define CF_ERROR_DS_CREATE_CTX				CF_ERROR_DS - 4
#define CF_ERROR_DS_CREATE_NODE				CF_ERROR_DS - 5
#define CF_ERROR_DS_NO_MORE					CF_ERROR_DS - 6
/* }}} data structure - list/queue/stack */

/* bitwise {{{ */
#define CF_ERROR_BITWISE					CF_ERROR_BASE * 9
/*------------------------------------------------------------*/
#define CF_ERROR_BITWISE_INVALID_ARGS		CF_ERROR_BITWISE - 1
#define CF_ERROR_BITWISE_ALLOCATE_BUFFER	CF_ERROR_BITWISE - 2
/* }}} bitwise */

/* util {{{ */
#define CF_ERROR_UTIL						CF_ERROR_BASE * 10
/*------------------------------------------------------------*/
#define CF_ERROR_UTIL_INVALID_ARGS			CF_ERROR_UTIL - 1
#define CF_ERROR_UTIL_GET_LOCALTIME			CF_ERROR_UTIL - 2
/* }}} util */

#endif // #ifndef __CF_ERROR_H__
