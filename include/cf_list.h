/**
 * \file cf_list.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 연결 리스트
 *
 * \example list.c
 */
#ifndef __CF_LIST_H__
#define __CF_LIST_H__

#include "cf_base.h"

/** 리스트 탐색자 (Opaque) */
typedef cf_ctx	cf_traverser;

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_List_Create			(cf_ctx * ctx);

CF_EXPORT int
CF_List_Destroy			(cf_ctx ctx);

CF_EXPORT int
CF_List_Front			(cf_ctx			ctx,
						 cf_traverser	* traverser);

CF_EXPORT int
CF_List_Rear			(cf_ctx			ctx,
						 cf_traverser	* traverser);

CF_EXPORT int
CF_List_AddFront		(cf_ctx		ctx,
						 const void	* element);

CF_EXPORT int
CF_List_AddRear			(cf_ctx		ctx,
						 const void	* element);

CF_EXPORT int
CF_List_InsertBefore	(cf_ctx				ctx,
						 const cf_traverser	traverser,
						 const void			* element);

CF_EXPORT int
CF_List_InsertAfter		(cf_ctx				ctx,
						 const cf_traverser	traverser,
						 const void			* element);

CF_EXPORT int
CF_List_Set				(cf_traverser	traverser,
						 const void		* element);

CF_EXPORT int
CF_List_Get				(const cf_traverser	traverser,
						 void				** element);

CF_EXPORT int
CF_List_Remove			(cf_ctx			ctx,
						 cf_traverser	* traverser);

CF_EXPORT int
CF_List_RemoveAll		(cf_ctx ctx);

CF_EXPORT int
CF_List_Prev			(cf_traverser * traverser);

CF_EXPORT int
CF_List_Next			(cf_traverser * traverser);

CF_EXPORT int
CF_List_GetSize			(cf_ctx ctx);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_LIST_H__
