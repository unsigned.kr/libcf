/**
 * \file cf_socket.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief TCP 소켓 지원
 *
 * \example socket.c
 */
#ifndef __CF_SOCKET_H__
#define __CF_SOCKET_H__

#include "cf_base.h"

#include <stddef.h>

/** 타임아웃을 설정하지 않음 */
#define CF_SOCKET_NO_TIMEOUT		0

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Socket_Initialize		(void);

CF_EXPORT int
CF_Socket_Finalize			(void);

CF_EXPORT int
CF_Socket_Create			(cf_ctx * ctx);

CF_EXPORT int
CF_Socket_Close				(cf_ctx ctx);

CF_EXPORT int
CF_Socket_Connect			(cf_ctx					ctx,
							 const char				* ip,
							 const unsigned short	port);

CF_EXPORT int
CF_Socket_Server			(cf_ctx					ctx,
							 const unsigned short	port,
							 const int				backlog);

CF_EXPORT int
CF_Socket_Accept			(const cf_ctx	ctx,
							 cf_ctx			* client);

CF_EXPORT int
CF_Socket_Send				(const cf_ctx	ctx,
							 const void		* buf,
							 const size_t	len);

CF_EXPORT int
CF_Socket_Recv				(const cf_ctx	ctx,
							 void			* buf,
							 const size_t	len);

CF_EXPORT int
CF_Socket_SetOption			(const cf_ctx	ctx,
							 const int		optname,
							 const void		* optval,
							 const size_t	optlen);

CF_EXPORT int
CF_Socket_GetOption			(const cf_ctx	ctx,
							 const int		optname,
							 void			* optval,
							 size_t			* optlen);

CF_EXPORT int
CF_Socket_SetTimeOut		(cf_ctx	ctx,
							 int	timeout);

CF_EXPORT int
CF_Socket_GetIP				(const cf_ctx	ctx,
							 char			* ip);

CF_EXPORT int
CF_Socket_GetPort			(const cf_ctx	ctx,
							 unsigned short	* port);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_SOCKET_H__
