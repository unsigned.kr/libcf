/**
 * \file cf_queue.h
 *
 * \author myusgun <myusgun@gmail.com>
 *
 * \brief 큐
 *
 * \example queue.c
 */
#ifndef __CF_QUEUE_H__
#define __CF_QUEUE_H__

#include "cf_base.h"

#ifdef __cplusplus
extern "C" {
#endif

CF_EXPORT int
CF_Queue_Create			(cf_ctx * ctx);

CF_EXPORT int
CF_Queue_Destroy		(cf_ctx ctx);

CF_EXPORT int
CF_Queue_Put			(cf_ctx		ctx,
						 const void	* element);

CF_EXPORT int
CF_Queue_Get			(cf_ctx	ctx,
						 void	** element);

CF_EXPORT int
CF_Queue_Front			(cf_ctx	ctx,
						 void	** element);

CF_EXPORT int
CF_Queue_GetSize		(cf_ctx ctx);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __CF_QUEUE_H__
