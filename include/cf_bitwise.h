/**
 * \file cf_bitwise.h
 *
 * \author myusgun <myusgun@gmail.com>
 * \author chevalier <achagun@gmail.com>
 *
 * \brief 비트 연산자
 *
 * \example bitwise.c
 */
#ifndef __CF_BITWISE_H__
#define __CF_BITWISE_H__

#include "cf_base.h"
#include "cf_type.h"

#include <stddef.h>

CF_EXPORT int
CF_Bitwise_ShiftLeft		(cf_byte	* in,
							 size_t		size,
							 size_t		offset,
							 cf_byte	* out);
CF_EXPORT int
CF_Bitwise_ShiftRight		(cf_byte	* in,
							 size_t		size,
							 size_t		offset,
							 cf_byte	* out);

CF_EXPORT int
CF_Bitwise_RotateLeft		(cf_byte	* in,
							 size_t		size,
							 size_t		offset,
							 cf_byte	* out);

CF_EXPORT int
CF_Bitwise_RotateRight		(cf_byte	* in,
							 size_t		size,
							 size_t		offset,
							 cf_byte	* out);

CF_EXPORT int
CF_Bitwise_AND				(cf_byte	* in1,
							 cf_byte	* in2,
							 size_t		size,
							 cf_byte	* out);

CF_EXPORT int
CF_Bitwise_OR				(cf_byte	* in1,
							 cf_byte	* in2,
							 size_t		size,
							 cf_byte	* out);

CF_EXPORT int
CF_Bitwise_XOR				(cf_byte	* in1,
							 cf_byte	* in2,
							 size_t		size,
							 cf_byte	* out);

#endif // #ifndef __CF_BITWISE_H__
